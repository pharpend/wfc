%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
-module(wfc).

-export_type([sentence/0]).

-export([pp/1, pf/1, sum/1, prod/1, eval/2]).
-export([wxor/2, wand/2, wnot/1, wrc/2, wior/2,
         wimplies/2, wimpliedby/2, wiff/2]).
-export([wcap/2, wcup/2]).

-type sentence() :: wfc_sentence:sentence().
-type word() :: wfc_word:word().
-type wfchar() :: 0 | 1 | atom().
-type wf() :: wfchar() | word() | sentence().

-spec pp(sentence()) -> ok.
-spec pf(sentence()) -> iolist().
-spec eval(sentence(), #{atom() := 0 | 1}) -> 0 | 1.
-spec sum([wf()]) -> sentence().
-spec prod([wf()]) -> sentence().


eval(Sentence, Map) ->
    wfc_sentence:eval(Sentence, Map).

pp(Sentence) ->
    io:format("~ts~n", [pf(Sentence)]).

pf(Sentence) ->
    unicode:characters_to_list(wfc_sentence:pf(Sentence)).

sum(Items) ->
    sum(Items, wfc_sentence:zero()).


sum([], Accum) ->
    Accum;
sum([0 | Rest], Accum) ->
    sum(Rest, Accum);
sum([1 | Rest], Accum) ->
    Sent1 = wfc_sentence:from_wfchar(1),
    NewAccum = wfc_sentence:plus(Sent1, Accum),
    sum(Rest, NewAccum);
sum([Atom | Rest], Accum) when is_atom(Atom) ->
    Sent = wfc_sentence:from_wfchar(Atom),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum);
sum([Word = {w,_} | Rest], Accum) ->
    true = wfc_word:is_valid_word(Word),
    Sent = wfc_sentence:from_word(Word),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum);
sum([Sent = {s,_} | Rest], Accum) ->
    true = wfc_sentence:is_valid_sentence(Sent),
    NewAccum = wfc_sentence:plus(Sent, Accum),
    sum(Rest, NewAccum).


prod(Items) ->
    prod(Items, wfc_sentence:one()).


prod([], Accum) ->
    Accum;
prod([0 | _Rest], _Accum) ->
    wfc_sentence:zero();
prod([1 | Rest], Accum) ->
    prod(Rest, Accum);
prod([Atom | Rest], Accum) when is_atom(Atom) ->
    Sent = wfc_sentence:from_wfchar(Atom),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum);
prod([Word = {w,_} | Rest], Accum) ->
    true = wfc_word:is_valid_word(Word),
    Sent = wfc_sentence:from_word(Word),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum);
prod([Sent = {s,_} | Rest], Accum) ->
    true = wfc_sentence:is_valid_sentence(Sent),
    NewAccum = wfc_sentence:times(Sent, Accum),
    prod(Rest, NewAccum).


% connectives, just hardcoded
wxor(A, B) ->
    sum([A, B]).

wand(A, B) ->
    prod([A, B]).

wnot(A) ->
    sum([1, A]).

wrc(A, B) ->
    sum([A, prod([A, B])]).

wior(A, B) ->
    sum([A, B, prod([A, B])]).

wimplies(A, B) ->
    sum([1, A, prod([A, B])]).

wimpliedby(A, B) ->
    sum([1, B, prod([A, B])]).

wiff(A, B) ->
    sum([1, A, B]).


%% connectives of set theory:
wcap(A, B) -> wand(A, B).
wcup(A, B) -> wior(A, B).
