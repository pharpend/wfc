%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, The Founder
%
% a word is the smallest unit in a reduced sum. In for instance
%
% 1 + A + AB, the words are 1, A, and AB, which are represented as
% the sets {}, {a}, and {a, b}, respectively.
%
% a word is a tagged-tuple {w, <set of atoms>}; the empty set equals 1
%
% in WF algebra, anything times itself equals itself, therefore we
% don't need to keep track of exponents. That is why the set
% representation makes sense.
%
% with a word, multiplication is implied
% with a sentence, summation is implied
-module(wfc_word).

-export_type([
    map01/0,
    wfchar/0,
    word/0
]).
-export([
    eval/2,
    from_list/1,
    is_one/1,
    is_valid_word/1,
    one/0,
    pf/1,
    times/2,
    to_list/1
]).

-type map01()  :: #{atom() := 0 | 1}.
-type wfchar() :: atom().
-type word()   :: {w, sets:set(wfchar())}.

%%% API

-spec eval(word(), map01()) -> 0 | 1.
% @doc
% evaluate a word; given a word, and a map that maps every atom to 1
% or 0, take the product of all the atoms in the word

eval({w, Set}, Map) ->
    Folder =
        fun(Atom, Accum) ->
            Val01 = maps:get(Atom, Map),
            NewAccum = Val01 * Accum,
            NewAccum
        end,
    sets:fold(Folder, 1, Set).



-spec from_list([wfchar()]) -> word().
% @doc
% Given a list of atoms, take their "product" and put it into a
% word; in practice, this means calling sets:from_list/1, and
% returning {w, Set}.

from_list(List) ->
    Set = sets:from_list(List),
    Word = {w, Set},
    true = is_valid_word(Word),
    Word.



-spec is_one(term()) -> boolean().
% @doc a word is one if it {w, EmptySet}.

is_one(Word) ->
    Word =:= one().



-spec is_valid_word(term()) -> boolean().
% @doc
% a word is valid if exactly one of these conditions are true:
%
%   - is empty
%   - contains only atoms
%
% return false on anything failing to pattern match {w, Set}

is_valid_word({w, Set}) ->
    Pred =
        fun(Element, Accumulator) ->
            Accumulator andalso is_atom(Element)
        end,
    sets:fold(Pred, true, Set);
is_valid_word(_) ->
    false.



-spec one() -> word().
% @doc The word corresponding to the concept "1"; it is a tagged
% tuple of {w, EmptySet}.

one() ->
    {w, sets:new()}.



-spec pf(word()) -> iolist().
% @doc
% returns iolist
%
% "(*)"         if word is 1
% "(* a b c)"   if word is the set containing {a,b,c}

pf(Word) ->
    true = is_valid_word(Word),
    IsOne = is_one(Word),
    Strs =
        if
            IsOne ->
                "";
            true ->
                Atoms = to_list(Word),
                pf_atoms(Atoms, [])
        end,
    ["(*", Strs, ")"].



-spec times(word(), word()) -> word().
% @doc
% Multiply two words (take the union of the atoms contained in the
% second item of the respective tuples)
% @end


% multiplication of two words is just the union; save for removing
% one
%
% n.b. this can probably be optimized since I fixed the stupid
% representation issue with 1 being represented as {w, set(1)}.
times({w, L}, {w, R}) ->
    % take the unions of the things it contains
    LR = sets:union(L, R),
    Word = {w, LR},
    true = is_valid_word(Word),
    Word.



-spec to_list(word()) -> [wfchar()].
% @doc
% pull out the set in the tagged tuple, convert it to a list, and
% return the sorted list

to_list({w, Set}) ->
    lists:sort(sets:to_list(Set)).



%%% Internals

-spec pf_atoms([atom()], Accum :: iolist()) -> iolist().
% @private
% pretty-print list of atoms with spaces before each one

pf_atoms([], Accum) ->
    Accum;
pf_atoms([A | As], Accum) ->
    S = atom_to_list(A),
    NewAccum = [Accum, " ", S],
    pf_atoms(As, NewAccum).
