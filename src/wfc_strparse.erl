-module(wfc_strparse).

%-export([
%    strparse/1
%]).

-compile(export_all).


-type lcchar() :: $a .. $z.
-type lcatom() :: [lcchar()].
-type wfchar() :: 0 | 1 | lcatom().
-type wfop()   :: '+' | '*' | lcatom().

-type wfsexp() :: {wfsexp, wfop(), [wfexpr()]}.

-type wfexpr() :: wfsexp() | wfchar() | wfop().

-type parser_result() :: {error, Reason :: term()}
                       | {ok, wfexpr(), Remainder :: string()}.


-spec readeval(string()) -> wfc:sentence().

readeval(S) ->
    {ok, Tree, ""} = read(S),
    eval(Tree).



-spec ref(string()) -> iolist().
% @doc "read eval format"

ref(S) ->
    wfc:pf(readeval(S)).



-spec ref_safe(string()) -> {ok, iolist()} | {error, Reason :: any()}.
% @doc "read eval format" but catches errors

ref_safe(S) ->
    try ref(S) of
        Result ->
            {ok, Result}
    catch
        error:X ->
            {error, X}
    end.




-spec rep(string()) -> ok.
% @doc "read eval print"

rep(S) ->
    wfc:pp(readeval(S)).




-spec read(string()) -> parser_result().

read(Str = [$( | _]) ->
    sexp(Str);
% 0, 1, +, and * are atomic special snowflakes
read([$0 | Rest]) ->
    {ok, 0, Rest};
read([$1 | Rest]) ->
    {ok, 1, Rest};
read([$+ | Rest]) ->
    {ok, '+', Rest};
read([$* | Rest]) ->
    {ok, '*', Rest};
% lowercase
read(Str = [C | _]) when $a =< C, C =< $z ->
    lcatom(Str);
read(X) ->
    {error, [read, invalid_value, X]}.



-spec eval(wfexpr()) -> wfc:sentence().

eval(X) ->
    eval_arg(X).



-spec eval_sexp(wfsexp()) -> wfc:sentence().

eval_sexp({wfsexp, OpS, ArgSs}) ->
    Op = eval_op(OpS),
    Args = lists:map(fun eval_arg/1, ArgSs),
    Op(Args).



-spec eval_arg(wfexpr()) -> wfc:sentence().

eval_arg(0) ->
    wfc:sum([]);
eval_arg(1) ->
    wfc:prod([]);
eval_arg(Sexp = {wfsexp, _, _}) ->
    eval_sexp(Sexp);
% case when argument is an atom
eval_arg(Str) when erlang:is_list(Str) ->
    erlang:list_to_atom(Str).



-spec eval_op(wfop()) -> fun().

% the ones that mean and
eval_op('*') -> fun wfc_listfuns:land/1;
eval_op("and") -> fun wfc_listfuns:land/1;
eval_op("intersection") -> fun wfc_listfuns:land/1;
% the ones that mean "iff"
eval_op("iff") -> fun wfc_listfuns:liff/1;
eval_op("eqset") -> fun wfc_listfuns:liff/1;
% the ones that mean "implies"
eval_op("implies") -> fun wfc_listfuns:limplies/1;
eval_op("subset") -> fun wfc_listfuns:limplies/1;
% the ones that mean "impliedby"
eval_op("impliedby") -> fun wfc_listfuns:limpliedby/1;
eval_op("supset") -> fun wfc_listfuns:limpliedby/1;
% the ones that mean "ior"
eval_op("ior") -> fun wfc_listfuns:lior/1;
eval_op("union") -> fun wfc_listfuns:lior/1;
% the ones that mean not
eval_op("not") -> fun wfc_listfuns:lnot/1;
eval_op("complement") -> fun wfc_listfuns:lnot/1;
% the ones that mean xor
eval_op('+') -> fun wfc_listfuns:lxor/1;
eval_op("symdiff") -> fun wfc_listfuns:lxor/1;
eval_op("xor") -> fun wfc_listfuns:lxor/1.



-spec sexp(string()) -> Result
    when Result :: {ok, wfsexp(), Rest}
                 | {error, Reason},
         Rest :: string(),
         Reason :: any().

sexp(String) ->
    % ackshully
    % consume beginning
    % consume operator
    % consume operands
    % consume end
    {ok, Rest0}           = whitespace(String),
    {ok, Rest1}           = sexp_start(Rest0),
    {ok, Rest2}           = whitespace(Rest1),
    {ok, Operator, Rest3} = sexp_operator(Rest2),
    {ok, Rest4}           = whitespace(Rest3),
    {ok, Operands, Rest5} = sexp_operands(Rest4),
    {ok, Rest6}           = sexp_end(Rest5),
    {ok, {wfsexp, Operator, Operands}, Rest6}.



-spec sexp_start(string()) -> Result
            when Result :: {ok, Rest :: string()}
                         | {error, Reason :: term()}.

sexp_start([$( | Rest]) -> {ok, Rest};
sexp_start(X)           -> {error, [sexp_start, invalid_value, X]}.



-spec sexp_operator(string()) -> Result
            when Result :: {ok, Operator :: wfop(), Rest :: string()}
                         | {error, Reason :: term()}.
% @doc
% consume the operator in an s-expression; doesn't consume following
% space or the paren

sexp_operator(String) ->
    sexp_operator(String, "").



-spec sexp_operator(ParseStr, Accum) -> Result
            when ParseStr :: string(),
                 Accum    :: string(),
                 Result   :: {ok, Operator, Remainder}
                           | {error, Reason},
                 Operator :: wfop(),
                 Remainder :: string(),
                 Reason :: any().
% @private
% An operator can either be +, *, or a string of lowercase letters

% + or * are special snowflakes
sexp_operator([$+ | Rest], []) ->
    {ok, '+', Rest};
sexp_operator([$* | Rest], []) ->
    {ok, '*', Rest};
% empty sexp is an error
sexp_operator(S = [$) | _], []) ->
    {error, [sexp_operator, empty_sexp, S]};
% allow initial spaces
sexp_operator([$  | Rest], []) ->
    sexp_operator(Rest, []);
% not the initial spaces case, reverse list and convert to atom
sexp_operator(Rest = [$  | _], Accum) ->
    Operator = lists:reverse(Accum),
    {ok, Operator, Rest};
% end of sexp
sexp_operator(Rest = [$) | _], Accum) ->
    OpStr = lists:reverse(Accum),
    {ok, OpStr, Rest};
% eat moar characters
% alphabetical character
sexp_operator([C | Rest], Accum) when $a =< C, C =< $z ->
    sexp_operator(Rest, [C | Accum]).



-spec sexp_operands(string()) -> {ok, Operands, Rest} | {error, Reason}
            when Operands :: [wfexpr()],
                 Rest     :: string(),
                 Reason   :: any().

sexp_operands(Str) ->
    sexp_operands(Str, []).



-spec sexp_operands(Input, Accum) -> Result
            when Result   :: {ok, Operands, Rest}
                           | {error, Reason},
                 Input    :: string(),
                 Accum    :: Operands,
                 Operands :: [wfexpr()],
                 Rest     :: string(),
                 Reason   :: any().

% End of sexp
sexp_operands(Rest = [$) | _], OperandsRev) ->
    {ok, lists:reverse(OperandsRev), Rest};
% space, keep going
sexp_operands([$  | Rest], OperandsRev) ->
    sexp_operands(Rest, OperandsRev);
% Not a space or close paren, read it
sexp_operands(Input, AccumOperands) ->
    {ok, NewOperand, NewRest} = read(Input),
    sexp_operands(NewRest, [NewOperand | AccumOperands]).



-spec whitespace(string()) -> {ok, Rest :: string()}
                            | {error, Reason :: any()}.
% @private
% Consume 0 or more whitespace characters
% @end

whitespace([$  | Rest]) ->
    whitespace(Rest);
whitespace(NotSpace) ->
    {ok, NotSpace}.



-spec sexp_end(string()) -> {ok, Rest :: string()}
                          | {error, Reason :: any()}.
% @private
% Consume a closing paren
% @end

sexp_end([$) | Rest]) -> {ok, Rest};
sexp_end(X)           -> {error, [sexp_end, invalid_value, X]}.



-spec lcatom(Input) -> Result
            when Input :: string(),
                 Result :: {ok, LCAtom, Rest} | {error, Reason},
                 LCAtom :: lcatom(),
                 Rest :: string(),
                 Reason :: any().

lcatom(Str = [C | _]) when $a =< C, C =< $z ->
    lcatom(Str, []);
lcatom(X) ->
    {error, [lcatom, invalid_value, X]}.



-spec lcatom(Input, Accum) -> Result
            when Input :: string(),
                 Result :: {ok, LCAtom, Rest} | {error, Reason},
                 Accum :: string(),
                 LCAtom :: lcatom(),
                 Rest :: string(),
                 Reason :: any().

% lc atom must be closed by space, end, or close paren
lcatom([C | Rest], Accum) when $a =< C, C =< $z ->
    lcatom(Rest, [C | Accum]);
% closed by end
lcatom([], Accum) ->
    {ok, lists:reverse(Accum), []};
% closed by space
lcatom(Rest = [$  | _], Accum) ->
    {ok, lists:reverse(Accum), Rest};
% closed by close paren
lcatom(Rest = [$) | _], Accum) ->
    {ok, lists:reverse(Accum), Rest}.
